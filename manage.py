from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from db import Database

db = Database('ow_comp.db')

# Create window object
app = Tk()

# Commands
def populate_list():
    games_list.delete(0, END)
    for row in db.fetch():
        games_list.insert(END, row)
        
def add_game():

    db.insert(
        gm_entry.get(), 
        map_entry.get(),
        role_picked.get(),
        hero1_picked.get(), 
        hero2_picked.get(), 
        result_entry.get(),
        team_score_entry.get(),
        enemy_score_entry.get(),
        duration_text.get(),
        sr_entry.get()
    ),
    games_list.delete(0, END)
    games_list.insert(
        END,
        (
            gm_entry.get(), 
            map_entry.get(),
            role_picked.get(),
            hero1_picked.get(),
            hero2_picked.get(),  
            result_entry.get(),
            team_score_entry.get(),
            enemy_score_entry.get(),
            duration_text.get(),
            sr_entry.get()
        )
    )
    clear_text()
    populate_list()

def select_game(event):
    try:
        global selected_game
        index = games_list.curselection()[0]
        selected_game = games_list.get(index)
        gm_entry.delete(0, END)
        gm_entry.insert(END, selected_game[1])
        map_entry.delete(0, END)
        map_entry.insert(END, selected_game[2])
        role_picked.delete(0, END)
        role_picked.insert(END, selected_game[3])
        hero1_picked.delete(0, END)
        hero1_picked.insert(END, selected_game[4])
        hero2_picked.delete(0, END)
        hero2_picked.insert(END, selected_game[5])
        result_entry.delete(0, END)
        result_entry.insert(END, selected_game[6])
        team_score_entry.delete(0, END)
        team_score_entry.insert(END, selected_game[7])
        enemy_score_entry.delete(0, END)
        enemy_score_entry.insert(END, selected_game[8])
        duration_entry.delete(0, END)
        duration_entry.insert(END, selected_game[9])
        sr_entry.delete(0, END)
        sr_entry.insert(END, selected_game[10])
    except IndexError:
        pass

def remove_game():
    db.remove(selected_game[0])
    clear_text()
    populate_list()

# def update_game():
#     db.update(
#         selected_game[0], 
#         gm_text.get(), 
#         map_text.get(), 
#         role_picked_text.get(),
#         hero1_picked_text.get(),
#         hero2_picked_text.get(),
#         result_text.get(), 
#         team_score_text.get(),
#         enemy_score_text.get(),
#         duration_text.get(),
#         sr_text.get()
#     )
#     populate_list()

def clear_text():
    gm_entry.delete(0, END)
    map_entry.delete(0, END)
    role_picked.delete(0, END)
    hero1_picked.delete(0, END)
    hero2_picked.delete(0, END)
    result_entry.delete(0, END)
    team_score_entry.delete(0, END)
    enemy_score_entry.delete(0, END)
    duration_entry.delete(0, END)
    sr_entry.delete(0, END)


# Lists
role_list = [
    'Tank',
    'Damage',
    'Support'
]
tank_list = [
    'D.va',
    'Orisa',
    'Reinhardt',
    'Roadhog',
    'Sigma',
    'Winston',
    'Wrecking Ball',
    'Zarya'
]
damage_list = [
    'Ashe',
    'Bastion',
    'Doomfist',
    'Echo',
    'Genji',
    'Hanzo',
    'Junkrat',
    'McCree',
    'Mei',
    'Pharah',
    'Reaper',
    'Soldier: 76',
    'Sombra',
    'Symmetra',
    'Torbjörn',
    'Tracer',
    'Widowmaker'
]
support_list = [
    'Ana',
    'Baptiste',
    'Brigitte',
    'Lúcio',
    'Mercy',
    'Moira',
    'Zenyatta'
]
hero_list = [
    tank_list,
    damage_list, 
    support_list
]
mode_list = [
    'Assault',
    'Control',
    'Escort',
    'Hybrid'
]
control_maps = [
    'Busan',
    'Ilios',
    'Lijiang Tower',
    'Nepal',
    'Oasis'
]
assault_maps = [
    'Hanamura',
    'Horizon Lunar Colony',
    'Paris',
    'Temple of Anubis',
    'Volskaya Industries'
]
escort_maps = [
    'Dorado',
    'Havana',
    'Junkertown',
    'Rialto',
    'Route 66',
    'Watchpoint: Gibraltar'
]
hybrid_maps = [
    'Blizzard World',
    'Eichenwalde',
    'Hollywood',
    "King's Row",
    'Numbani'
]
maps_list = [
    assault_maps,
    control_maps,
    escort_maps,
    hybrid_maps
]
result_list = [
    'Win',
    'Loss',
    'Tie',
    'Undecided'
]

# Game Mode
gm_text = StringVar()
gm_label = Label(app, text='Game mode')
gm_label.grid(row=0, column=0, sticky='w')
gm_entry = ttk.Combobox(app, width=37, value=(mode_list))
gm_entry.grid(row=0, column=1, columnspan=2, padx=10, pady=2, sticky='w')

def mode_callback(eventObject):
    abc = eventObject.widget.get()
    index = mode_list.index(gm_entry.get())
    map_entry.config(values=maps_list[index])

# Map
map_text = StringVar()
map_label = Label(app, text='Map')
map_label.grid(row=1, column=0, sticky='w')
map_entry = ttk.Combobox(app, width=37)
map_entry.grid(row=1, column=1, columnspan=2, padx=10, pady=2, sticky='w')
map_entry.bind('<Button-1>', mode_callback)

# Role
role_picked_text = StringVar()
role_picked_label = Label(app, text='Role')
role_picked_label.grid(row=2, column=0, sticky='w')
role_picked = ttk.Combobox(app, width=37, value=(role_list))
role_picked.grid(row=2, column=1, columnspan=2, padx=10, pady=2, sticky='w')

def role_callback(eventObject):
    abc = eventObject.widget.get()
    index = role_list.index(role_picked.get())
    hero1_picked.config(values=hero_list[index])
    hero2_picked.config(value=hero_list[index])

# Hero 1
hero1_picked_text = StringVar()
hero1_picked_label = Label(app, text='Hero 1')
hero1_picked_label.grid(row=3, column=0, sticky='w')

hero1_picked = ttk.Combobox(app, width=27)
hero1_picked.grid(row=3, column=1, columnspan=1, padx=10, pady=2, sticky='w')
hero1_picked.bind('<Button-1>', role_callback)

# Hero 2
hero2_picked_text = StringVar()
hero2_picked_label = Label(app, text='Hero 2')
hero2_picked_label.grid(row=4, column=0, sticky='w')

hero2_picked = ttk.Combobox(app, width=27)
hero2_picked.grid(row=4, column=1, columnspan=1, padx=10, pady=2, sticky='w')
hero2_picked.bind('<Button-1>', role_callback)

# Result
result_text = StringVar()
result_label = Label(app, text='Result')
result_label.grid(row=5, column=0, sticky='w')
result_entry = ttk.Combobox(app, width=27, value=(result_list))
result_entry.grid(row=5, column=1, columnspan=2, padx=10, pady=2, sticky='w')

# Scores
score_label = Label(app, text='Score')
score_label.grid(row=5, column=2)
to_label = Label(app, text='to')
to_label.grid(row=5, column=4, sticky='w')

team_score_text = StringVar()
team_score_entry = Entry(app, width=7, textvariable=team_score_text)
team_score_entry.grid(row=5, column=3)

enemy_score_text = StringVar()
enemy_score_entry = Entry(app, width=7, textvariable=enemy_score_text)
enemy_score_entry.grid(row=5, column=5)

# Duration
duration_text = StringVar()
duration_label = Label(app, text='Duration')
duration_label.grid(row=7, column=0, sticky='w')

duration_entry = Entry(app, width=37, textvariable=duration_text)
duration_entry.grid(row=7, column=1)

# Skill Rating
sr_text = StringVar()
sr_label = Label(app, text='Skill rating')
sr_label.grid(row=8, column=0, sticky='w')

sr_entry = Entry(app, width=37, textvariable=sr_text)
sr_entry.grid(row=8, column=1)

# Listbox
games_list = Listbox(app, height=8, width=80)
games_list.grid(row=9, column=0, columnspan=3, rowspan=6, pady=20, padx=20)

# Bind select
games_list.bind('<<ListboxSelect>>', select_game)


# Buttons
add_button = Button(
    app, 
    text='Add game', 
    width=12, 
    command=add_game
)
add_button.grid(row=16, column=0)

remove_button = Button(
    app, 
    text='Remove game', 
    width=12, 
    command=remove_game
)
remove_button.grid(row=16, column=1)

# update_button = Button(
#     app, 
#     text='Update game', 
#     width=12, 
#     command=update_game
# )
# update_button.grid(row=17, column=0)

clear_button = Button(
    app, 
    text='Clear input', 
    width=12, 
    command=clear_text
)
clear_button.grid(row=17, column=1)

# Title and dimensions
app.title('Overwatch game tracker')
app.geometry('800x480')

# Start program
app.mainloop()