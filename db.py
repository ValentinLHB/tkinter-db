import sqlite3

class Database:
    def __init__(self, db):
        self.conn = sqlite3.connect(db)
        self.cur = self.conn.cursor()
        self.cur.execute(
            """
            CREATE TABLE IF NOT EXISTS games
            (
                id INTEGER PRIMARY KEY,
                mode text,
                map_name text,
                role text,
                hero1 text,
                hero2 text,
                result text,
                team_score text,
                enemy_score text,
                duration text,
                sr text
            )
            """
        )
        self.conn.commit()
    
    def fetch(self):
        self.cur.execute(
            "SELECT * FROM games"
        )
        rows = self.cur.fetchall()
        return rows

    def insert(self, mode, map_name, role, hero1, hero2, result, team_score, enemy_score, duration, sr):
        self.cur.execute(
            "INSERT INTO games VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            (mode, map_name, role, hero1, hero2, result, team_score, enemy_score, duration, sr)
        )
        self.conn.commit()
    
    def remove(self, id):
        self.cur.execute(
            "DELETE FROM games WHERE id=?", (id,)
        )
        self.conn.commit()

    # def update(self, id, mode, map_name, role, hero1, hero2, result, team_score, enemy_score, duration, sr):
    #     self.cur.execute(
    #         "UPDATE games SET mode = ?, map_name = ?, role = ?, hero1 = ?, hero2 = ?, result = ?, team_score = ?, enemy_score = ?, duration = ?, sr = ? WHERE id = ?",
    #         (mode, map_name, role, hero1, hero2, result, team_score, enemy_score, duration, sr)
    #     )
    #     self.conn.commit()
    
    def __del__(self):
        self.conn.close()

db = Database('ow_comp.db')

