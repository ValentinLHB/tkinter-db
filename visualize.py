import sqlite3
import pandas
from pandasgui import show

data = sqlite3.connect('ow_comp.db')

query = data.execute("SELECT * FROM games")

df = pandas.DataFrame.from_records(
    data = query.fetchall(), 
    columns = [column[0] for column in query.description]
)

gui = show(df)
